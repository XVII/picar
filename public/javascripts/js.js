App = function() {};

App.prototype.makeSwitches = function(elem) {
    var elem = $(elem);

    elem.on('click', '.item', function(e) {
        var _elem = $(this),
            data = $(this).attr('data'),
            set = !_elem.hasClass('active');

        app.api(data + '/' + (set ? 1 : 0), {}, function() {
            app.setSwitch(elem, data, set);
        });

    });

    elem.find('.item').each(function() {
        var data = $(this).attr('data');
        if (settings[data]) {
            $(this).addClass('active');
        }
    });
};

App.prototype.setSwitch = function(elem, data, set){
	$(elem).find('[data="' + data + '"]').toggleClass('active', set);
};

App.prototype.api = function(act, data, callback) {

    $.get('/api/' + act, data, function(res) {
        callback && callback(res) || console.log(res);
    });

};

app = new App();

app.makeSwitches('[menu="switches"]');







var socket = io.connect('http://avezof.ru:3001');
socket.on('', function(data) {
    console.log(data);
    socket.emit('my other event', {
        my: 'data'
    });
});