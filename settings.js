var express = require('express');
var jf = require('jsonfile'),
    util = require('util');

var Params = function(file){
  var self = this;

  this.file = file || '/tmp/file.json';

  jf.readFile(this.file, function(err, obj) {
    self.params = obj || {};
  });

};

Params.prototype.set = function(key, value){
  this.params[key] = value;
};

Params.prototype.get = function(key){
  return this.params[key];
};

Params.prototype.save = function(){
  jf.writeFile(this.file, this.params, function(err) {
    console.log(err);
  });
};

module.exports = new Params();
