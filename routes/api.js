var express = require('express');
var router = express.Router();
var settings = require('../settings');

router.get('/bagazh/:set', function(req, res, next) {
  var set = req.params.set;
  settings.set('bagazh', set);
  settings.save();
  res.send(set);
});

router.get('/rgb/:set', function(req, res, next) {
  var set = req.params.set;
  settings.set('rgb', set);
  settings.save();
  res.send(set);
});

router.get('/music/:set', function(req, res, next) {
  var set = req.params.set;
  settings.set('music', set);
  settings.save();
  res.send(set);
});

	router.get('/test', function(q,res,next){
		res.send(params.params);
	});

module.exports = router;
